package iamdev.me.ytds.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import iamdev.me.ytds.entity.EvernoteImport;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author zxc
 * @since 2018-07-04
 */
public interface EvernoteImportMapper extends BaseMapper<EvernoteImport> {

}
